
#### MQMesh UART透明代理 (默认参数)
./mq_mesh_bridge /tty/ttyUSB0 9600 8090

### 错误日志文件:
只有在出现错误时才输入,一般是程序退出后看这里
完整路径  /tmp/mq_mesh_err.log

### API

| 字段名        | 类型   | 含义                                            |
| ------------- | ------ | ----------------------------------------------- |
| tx            | uint64 | 发送累计 (自程序启动以来)                       |
| rx            | uint64 | 接收累计 (自程序启动以来)                       |
| wsState       | int    | Connecting = 0  Open = 1 Closing = 2 Closed = 3 |
| portIsOpen    | uint8  | 串口状态 ( open = 1  close = 0)                 |
| portDev       | string | 串口设备名                                      |
| portBaudSpeed | int32  | 串口波特率                                      |
| mcu           | string | MCU型号                                         |
| version       | string | 此程序的版本 (例:1.0.0)                         |



```
取实时状态:
GET http://127.0.0.1:8090/state
```

```
模组复位:
GET http://127.0.0.1:8090/reset
```

### 服务器端
地址: i.mqitv.com
端口: 6209
协议: Websocket over TLS
完整地址: wss://i.mqitv.com:6209/

### 串口
波特率: 9600
附加参数: Parity.None, 8, StopBits.One, Handshake.None

### MQMesh MCU (8266) --> 串口 --> Websocket
数据类型：char
数据是否已加密: 是
结尾符: \r\n (发往云端时要去掉结尾符)

### Websocket --> 串口 --> MQMesh MCU (8266)
数据：来自Websocket服务端数据 （直接转发）
数据是否已加密: 是
结尾符: \0 (如果有，直接转发; 如果没有，就由代理程序添加)

* connected\0  主机已连接网络通知
* reset\0            要求MCU恢复出厂配置
* bind\0             MCU进行绑定确认
* ping\0             检查MCU是否正确响应，回应带MCU的MeshId.   回应例: "pong->98465546"


### 注意事项:
* 在串口透明代理程序中，串口初始化完成后，主动发送一个消息通知 MCU 重新连接Websocket服务器
  消息内容: connected\0
  此消息是确保在代理程序后启动或重新启动时，MESH立即在线
  此消息不管发送多少，在一个周期只有一次有效

* 考虑到串口的速度一般慢于网络速度，双向收发数据时，建议做先进先出队列缓存

* Websocket的ping存活间隔小于 18 秒 (由代理程序维持)

* 程序在遇到通信异常时建议退出，从外部来守护进程

