
 ### 秒开MESH有线+无线桥节点

```
                    |-UART1-- ESP32-C3 (WiFi-STA)
 ESP32-C3(MQMesh)-- |
                    |-SPI-- W5500 (ETH)
```


#### 公用引脚

 ESP32(MQMesh)-C3 系统指示灯  => GPIO0    (低电平使能)
 ESP32(MQMesh)-C3 恢复出厂配置 => GPIO1   (低电平使能)


 ESP32-C3(WiFi-STA) 系统指示灯  => GPIO0


SPI     
 -------------------------
| ESP32C3(MQMesh) | W5500 | Memo |
| --------------- | ----- | ---- |
| GPIO3           | REST  |      |
| GPIO2           | MISO  |      |
| GPIO7           | MOSI  |      |
| GPIO10          | CS    | SS   |
| GPIO6           | SCLK  |      |




UART1 
----------------------------------------------------------------
| ESP32C3(MQMESH)  | ESP32-C3(WiFi-STA)   (TX/RX交叉) | Memo |
| ---------------- | -------------------------------- | ---- |
| UART1_TXD(GPIO4) | UART1_TXD(GPIO4)                 |      |
| UART1_RXD(GPIO5) | UART1_RXD(GPIO5)                 |      |
| RTS(GPIO18)      | RTS(GPIO18)                      |      |
| CTS(GPIO19)      | CTS(GPIO19)                      |      |




扩展接口
----------------------------------------------------------------

| ID   | Action | MCU                | GPIO | Memo |
| ---- | ------ | ------------------ | ---- | ---- |
| 1    | 干接点 | ESP32C3(MQMESH)    | 9    |      |
| 2    | 干接点 | ESP32C3(MQMESH)    | 11   |      |
| 3    | 干接点 | ESP32C3(WiFi-STA)  | 2    |      |
| 4    | 干接点 | ESP32-C3(WiFi-STA) | 3    |      |
| 5    | GND    |                    |      |      |
| 6    | 3V3    |                    |      |      |
| 7    | 干接点 | ESP32C3(WiFi-STA)  | 9    |      |
| 8    | 干接点 | ESP32-C3(WiFi-STA) | 11   |      |



-------------------------------------------------------------
 备注：
 -------------------------------------------------------------
 * 预留 ESP32C3调试刷机引脚 (GND/3V3/BOOT(GPIO9)/EN(RTS)/UART0_TXD/UART0_RXD)
 * 可能的话，板截一个门铃蜂鸣器 (由ESPC3_MQMESH-GPIO8 控制打开关闭)
 * 能在外壳上设计 恢复出厂配置 键（建议按键孔)
 * 选择性提供RJ45_PoE供电(非必须实现)
 * ESP32-C3最低存储要求(>=2Mbyte)
