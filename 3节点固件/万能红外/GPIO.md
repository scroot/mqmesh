#### 功能

- **功能不全**
- 当前采用先学习，再控制的方式

#### MCU

```
ESP8266
FLASH需要等于或大于2Mbyte(16Mbit)
```

#### 引脚定义 (默认固件对应此GPIO定义)

| 引脚功能     | 引脚   | 使能   | 备注 |
| ------------ | ------ | ------ | ---- |
| 恢复出厂配置 | ADC0   | 低电平 |      |
| 系统&指示灯  | GPIO0  | 低电平 |      |
| 红外收       | GPIO5  |        |      |
| 经外发       | GPIO14 |        |      |

