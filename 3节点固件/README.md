
#### 固件名称含义
```
firmware_ESP8266_switch86_mq_230_h1_v478.bin 
            |        |     |  |   |   |
          MCU型号  产品代号  |  |   |   |
                           |  |   | 软件版本 
                          厂家 |   |
                              |  硬件版本  
                            OEM代码  
```

#### 产品代号

| ID   | 名称         | 产品代号         | MCU           | 设备类型 | 备注   |
| ---- | ------------ | ---------------- | ------------- | -------- | ------ |
| 99   | 桥节点       | bridge           | ESP8266/ESP32 | Mesh节点 |        |
| 3    | 86型开关     | switch86         | ESP8266       | Mesh节点 |        |
| 4    | 插座         | plugin           | ESP8266       | Mesh节点 |        |
| 5    | 窗帘(纱)     | window           | ESP8266       | Mesh节点 |        |
| 6    | 万能红外     | ircontrol        | ESP8266       | Mesh节点 |        |
| 30   | 调光灯       | ledcontrol       | ESP8266       | Mesh节点 |        |
| *    | 插卡取电     | sensorcardswitch | ESP8266       | 传感器   | 开关型 |
| *    | 温湿度传感器 | sensordht        | ESP8266       | 传感器   | 值类型 |

