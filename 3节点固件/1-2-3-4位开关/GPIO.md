#### 功能要求

```
1、系统提示灯（背光）：系统灯在每个按钮处放置一到四颗，由系统灯IO0控制亮灭
2、按钮指示灯: 和继电器同步亮和灭,在每个按钮处放置一到四颗
3、恢复出厂配置按钮：方案1： 外部采用针孔按  方案2：打开盖子才能按
4、防MCU损坏设计（非必须实现）：目的[如果MCU损坏，开关正常能用]；
     技术方案[按钮和MCU IO两个高低电平输入到一个逻辑门输出到继电器 00:开 10:关 11:开 01:关
5、调试留孔(2.54mm)：保持一条线排列，分别是 RXD/TXD/RST/IO0/3V3/GND
6、8266天线处底板镂空 (最少要无走线)
```

#### MCU

```
ESP8266可采用多种模组布局： 1. ESP-WROOM-02D   2. ESP-12 3.秒开开源模组(推荐)
供电：采用强电和弱电分离，强电到弱电的转换推荐使用供电模组
FLASH需要等于或大于2Mbyte(16Mbit)
```

#### ESP8266 / 1-2-3-4开关引脚定义 (默认固件对应此GPIO定义)

| 引脚功能     | 引脚   | 使能       |
| ------------ | ------ | ---------- |
| 恢复出厂配置 | ADC0   | 低电平     |
| 系统&指示灯  | GPIO0  | 低电平     |
| 过零检测     | 无     |            |
| 按钮1        | GPIO12 | 低电平     |
| 继电器1      | GPIO13 | **高电平** |
| 按钮2        | GPIO1  | 低电平     |
| 继电器2      | GPIO5  | **高电平** |
| 按钮3        | GPIO3  | 低电平     |
| 继电器3      | GPIO4  | **高电平** |
| 按钮4        | GPIO2  | 低电平     |
| 继电器4      | GPIO14 | **高电平** |



#### ESP8266 / 1-2-3-4开关引脚定义 (有过零检测)
| 引脚功能     | 引脚   | 使能       |
| ------------ | ------ | ---------- |
| 恢复出厂配置 | ADC0   | 低电平     |
| 系统&指示灯  | GPIO0  | 低电平     |
| 过零检测     | GPIO4  |            |
| 按钮1        | GPIO3  | 低电平     |
| 继电器1      | GPIO14 | **高电平** |
| 按钮2        | GPIO5  | 低电平     |
| 继电器2      | GPIO12 | **高电平** |
| 按钮3        | GPIO2  | 低电平     |
| 继电器3      | GPIO13 | **高电平** |
| 按钮4        | GPIO16 | 低电平     |
| 继电器4      | GPIO15 | **高电平** |
