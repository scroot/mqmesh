# 秒开WiFi Mesh

# 个人用户完全免费

#### * 介绍
MQ-Mesh是基于ESP8266 MCU的2.4G WiFi 自研的Mesh组网，由武汉秒开网络历时二年开发；使得我们在蓝牙和zigbee之外多了一种组网的选择，MQ-Mesh组网已经通过了商业使用验证，可直接用于生产。

MQMesh*组网固件， 兼顾经济性和稳定性, 而且节点固件完全免费


```
云端登录： https://iot.meezhu.cn/
工厂产测:  https://iot.meezhu.cn/factory
APP下载:  https://morequick.net/download.html#iotapp

```

<img src=".res/iot_wxapp.jpeg" alt="微信小程序" width = "200" height = "200" alt="图片名称" />


##### * MQMesh

> 可以使用比ESP32更少资源的MCU

> TCP后端可靠传输，可以单播，也可以广播 

> 可以中继，自愈合，即插即用

> 通信全异步设计

> 彻底无中心化，节点真正对等

> 实现基于MESH的OTA (任何一个节点可以进入OTA）

> 实现基于MESH的配网 （任何一个节点可以进入配网)



#### * 组网拓扑

<img src=".res/mesh.png" alt="微信小程序" width = "500" height = "450" alt="图片名称" />



#### * 商业授权政策(个人用户免费)

> 桥节点由秒开提供硬件(软件授权再议)
>
> 节点固件免费授权
>
> 传感器固件免费授权



#### * 秒开官方测试套件

所有均已配置好的官方测试套件 [淘宝链接](https://item.taobao.com/item.htm?spm=a1z10.5-c.w4002-8288896779.11.52111bd3ih8Rk9&id=678307175848)



#### * 节点设备/传感设备, 采购推荐厂家

广东大明拉斐电气有限公司

深圳市智特尔电子有限公司

华展电子科技（西安）有限公司



