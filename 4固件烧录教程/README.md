### 8266刷机

```
TTL-USB_Driver(CH340) : 是Windows上USB转串口刷机工具的驱动，支持的型号为 CH340/341

flash_download_tool_x.x.x.zip : 是乐鑫官方提供的Windows上的刷机工具
```


 **硬件准备**: 

> * 秒开MQMesh固件的最低存储空间要求是 >= 2Mbyte

> * 需一要一个8266烧录器 （淘宝搜索关键字 "8266 烧录器" CH34x)
    需要一个8266开发板

> * 使用8266开发板 (ESP-WROOM-02D)，接入电脑，直接刷机即可测试



 [淘宝搜索8266开发板](https://s.taobao.com/search?q=esp8266%E5%BC%80%E5%8F%91%E6%9D%BF)




**烧录固件**:

#### 1.从官网下载刷机工具

[官网下载](https://www.espressif.com/)

[本地下载](../固件烧录教程/flash_download_tool_3.9.2_0.zip)


#### 2.运行后选择 ESP8266 

![输入图片说明](../.res/select.jpeg)

> 进入后选择下载的秒开MQMesh固件 (xxx.bin)

![输入图片说明](../.res/download.jpeg)

> START 开刷


#### 下图是针对秒开开关的刷机示意图
> * 把 GND / 3.3V / GPIO0 / RST / TXD / RXD 与烧录器对接好
