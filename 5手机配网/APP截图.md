配网过程按顺序图示: [点击下载APP](https://morequick.net/download.html#iotapp)

| <img src="../.res/01.jpeg" style="zoom:33%;" /> | <img src="../.res/02.jpeg" alt="66" style="zoom:33%;" /> | <img src="../.res/03.jpeg" alt="66" style="zoom:33%;" /> |
| ----------------------------------------------- | -------------------------------------------------------- | -------------------------------------------------------- |
| 账号登录                                        | 添加一个新的场所                                         | 新场所相关信息录入                                       |
| <img src="../.res/04.jpeg" style="zoom:33%;" /> | <img src="../.res/05.jpeg" style="zoom:33%;" />          | <img src="../.res/06.jpeg" style="zoom:33%;" />          |
| 添加房间和预置设备                              | 进入添加的房间                                           | 进入房间，准备发现设备                                   |
| <img src="../.res/07.jpeg" style="zoom:33%;" /> | <img src="../.res/08.jpeg" style="zoom:33%;" />          | -                                                        |
| 通过WiFi扫描新设备                              | 开始扫描&绑定                                            |                                                          |



