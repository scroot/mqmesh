

#### 云平台访问地址

```
https://iot.meezhu.cn/
```
[点击直达](https://iot.meezhu.cn/)


#### 下载APP

```
https://morequick.net/download.html#iotapp
```
[APP下载](https://morequick.net/download.html#iotapp)


#### 配网流程

** 第一阶段（在办公室操作） **

- APP登录
- 创建场所
- 创建房型
- 添加虚拟预置设备、添加设备联动关系、定义模式
- 根据房型创建房间

** 第二阶段(现场操作) **

- APP上进入房间
- 设备通电
- 扫描新设备列表
- 与虚拟预置设备绑定
- 完成